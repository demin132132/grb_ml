import copy
import warnings

import numpy as np
from scipy import interpolate

from base import *


def get_analysis():
    analysis = make_analysis.make_analysis()
    return analysis


def get_grid_dens(analysis, data_r=None, ax_limits=None, padding=None):

    if padding is None:
        padding = PADDING_DEFAULT

    if ax_limits is None:
        if data_r is None:
            ax_limits = AX_LIMITS_DEFAULT
            warnings.warn("neither ax_limits nor data_r supplied to get_grid_dens, falling back to default ax_limits.")
            x_grid, y_grid, z = get_z.get_z(
                model=analysis,
                **ax_limits,
                **padding
            )
        else:
            x_grid, y_grid, z = get_z.get_z(
                data=data_r,
                model=analysis,
                **padding
            )
    else:
        x_grid, y_grid, z = get_z.get_z(
            model=analysis,
            **ax_limits,
            **padding
        )

    x_grid = np.asarray(x_grid)
    y_grid = np.asarray(y_grid)
    z = np.asarray(z).T

    return x_grid, y_grid, z


def prob_by_cluster(analysis, x, y, family=None, num_clusters=None):
    if family is None:
        family = analysis.rx2('family')[0]
    if num_clusters is None:
        num_clusters = analysis.rx2('num_clusters')[0]
    pdf = PDF_BY_FAMILY[family]
    prob = np.array([pdf(x, y, analysis, j)[0] for j in range(1, num_clusters + 1)])
    prob = prob / prob.sum()
    return prob


def probs_to_levels(z, probs, n=1000):
    """

    Converts integral probabilities to contour levels for plotting.

    :param z: array-like (2D)
        Represents PDF calculated on a grid
    :param probs: array-like (1D)
        Contains integral probabilities for contour plotting; 0 < probs[i] < 1
    :param n: int
        Number of threshold values; higher values => more accurate levels
    :return: levels: np.ndarray (1D)
        Contour levels corresponding to the input
    """
    probs = np.array(probs)
    probs.sort()  # sorts in ascending order

    if not isinstance(z, np.ndarray):
        z = np.asarray(z)

    norm = z.sum()
    if not norm == 1:
        z = z / norm

    thresholds = np.linspace(0, z.max(), n)
    integral = ((z >= thresholds[:, None, None]) * z).sum(axis=(1, 2))
    f = interpolate.interp1d(integral, thresholds)
    levels = f(np.array(probs))

    return levels


def split_analysis(analysis):
    sub_analyses = []
    num_clusters = analysis.rx2('num_clusters')[0]
    for i in range(num_clusters):
        anls = copy.deepcopy(analysis)
        pii = [0.0] * num_clusters
        pii[i] += 1.0
        anls.rx2['pii'] = robjects.FloatVector(pii)
        sub_analyses.append(anls)
    return sub_analyses


def gen_cov():
    cov = genPositiveDefMat(
        dim=2,
        covMethod='eigen',
        rangeVar=c_r(0.1, 7)
    )
    print(cov)
