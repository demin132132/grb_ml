import os

import numpy as np
import pandas as pd
import rpy2.robjects

from tabulate import tabulate

from base import *
from rw_utils import write


np.set_printoptions(suppress=True)


def gen_md_text_one(analysis, heading=None):
    """
    :param analysis: rpy2.robjects.vectors.ListVector
    :param heading: str
        Does NOT include the '#' symbol.
    :return: md_contents: list[str]
    """
    family = analysis.rx2('family')[0]
    num_clusters = analysis.rx2('num_clusters')[0]

    md_contents = []
    if heading is not None:
        md_contents.append(f"# {heading}\n\n")

    md_contents.append(f"## {family} family, {num_clusters} clusters\n\n")
    md_contents.append(f"### Summary\n\n")

    num_points = analysis.rx2('n')[0]
    md_contents.append(fr"`num_points` = ${num_points}$<br/>")
    md_contents.append("\n")

    bic = analysis.rx2('bic')[0]
    bic = round(bic, 3)
    md_contents.append(fr"`bic` = ${bic}$<br/>")
    md_contents.append("\n")

    aic = analysis.rx2('aic')[0]
    aic = round(aic, 3)
    md_contents.append(fr"`aic` = ${aic}$<br/>")
    md_contents.append("\n")

    likelihood = analysis.rx2('logLik')[0]
    likelihood = round(likelihood, 3)
    md_contents.append(fr"`log-likelihood` = ${likelihood}$<br/>")
    md_contents.append("\n\n")

    for i in range(num_clusters):
        md_contents.append(fr"### Component # {i + 1}")
        md_contents.append("\n\n")

        pi = analysis.rx2('pii')[i]
        pi = np.asarray(pi).round(3)
        md_contents.append(fr"$\pi_{i + 1}$ = ${pi}$<br/>")
        md_contents.append("\n")

        mu = analysis.rx2('mu')[i]
        mu = np.asarray(mu).round(3)
        mu = f"({', '.join(map(str, mu))})"
        md_contents.append(fr"$\mu_{i + 1}$ = ${mu}$<br/>")
        md_contents.append("\n")

        sigma = analysis.rx2('Sigma')[i]
        sigma = np.asarray(sigma).round(3)
        sigma_tex_str = fr"""\begin{{pmatrix}}
{sigma[0, 0]} & {sigma[0, 1]} \\
{sigma[1, 0]} & {sigma[1, 1]} \\
\end{{pmatrix}}"""
        md_contents.append(fr"$\Sigma_{i + 1}$ = ${sigma_tex_str}$<br/>")  # .replace("\n", " "))
        md_contents.append("\n")

        if family.startswith("Skew"):
            shape = analysis.rx2('shape')[i]
            shape = c_r(shape)
            shape = np.asarray(shape).round(3)
            shape = f"({', '.join(map(str, shape))})"
            md_contents.append(fr"$\lambda_{i + 1}$ = ${shape}$<br/>")
            md_contents.append("\n")

        md_contents.append("\n")

    return md_contents


def gen_md(anls, filename, heading=None, out_path=ANALYSES_DIR):
    if isinstance(anls, rpy2.robjects.vectors.ListVector):
        md_contents = gen_md_text_one(analysis=anls, heading=heading)
    else:
        raise ValueError("Unsupported 'anls' type")

    if not filename.endswith('.md'):
        filename = f'{filename}.md'

    write(lines=md_contents, filename=filename, path=out_path)


def gen_table(analyses, filename, include_distrib_params=True, out_path=TABLE_DIR):

    if not filename.endswith('dat') or filename.endswith('txt'):
        filename = f'{filename}.dat'

    col_names = ['model', 'logLik', 'bic', 'aic']
    col_names_params = ['pii', 'mu', 'Sigma', 'shape', 'nu']

    if include_distrib_params:
        col_names.extend(col_names_params)

    data = []
    for analysis in analyses:
        num_clusters = analysis.rx2('num_clusters')[0]
        family = analysis.rx2('family')[0]
        model = f"{num_clusters}{SHORT_NAME_BY_FAMILY[family]}"
        row = [model]
        for col_name in col_names[1:]:
            if (col_name == 'shape' and 'S' not in model) or (col_name == 'nu' and 'T' not in model):
                item = '...'
            else:
                item = analysis.rx2(col_name)
                item = np.asarray(item).round(3)
                if len(item) == 1:
                    item = item[0]
                item = str(item).replace('\n\n', '\n')
            row.append(item)
        data.append(row)

    df = pd.DataFrame(data=data, columns=col_names)

    table = tabulate(df, df.columns, showindex=False)
    with open(os.path.join(out_path, filename), 'w') as wf:
        wf.write(table)
        wf.write('\n')
