import warnings

import pandas as pd
from random import random
import os

from base import *
from rw_utils import read_pickled
from utils import get_grid_dens
from fit import fit


def _sample(analysis, pdf, z, x_min, x_max, y_min, y_max, n_sample):
    # ceiling PDF: uniform distribution
    x_gen = []
    y_gen = []

    # general purpose:
    max_pdf = 2.0 * z.max()

    while len(x_gen) < n_sample:
        x = random() * (x_max - x_min) + x_min  # generate X coordinate
        y = random() * (y_max - y_min) + y_min  # generate Y coordinate
        h = random() * max_pdf  # generate Z coordinate (height)
        if h < pdf(float(x), float(y), analysis):  # accept if point is under the PDF surface, reject otherwise
            x_gen.append(x)
            y_gen.append(y)

    return x_gen, y_gen


def sample(analysis, n_sample, x_name=None, y_name=None, ax_limits=None):

    if x_name is None:
        x_name = 'x_feature'
    if y_name is None:
        y_name = 'y_feature'

    x_grid, y_grid, z = get_grid_dens(analysis, ax_limits=ax_limits)
    family = analysis.rx2('family')[0]

    x_gen, y_gen = _sample(
        analysis=analysis,
        pdf=PDF_MIXTURE_BY_FAMILY[family],
        z=z,
        x_min=min(x_grid),
        x_max=max(x_grid),
        y_min=min(y_grid),
        y_max=max(y_grid),
        n_sample=n_sample
    )

    data_gen = pd.DataFrame({
        x_name: x_gen,
        y_name: y_gen
    })

    n_digits = len(str(n_sample))
    names_gen = [f'GEN_{i:0{n_digits}}' for i in range(1, len(x_gen) + 1)]

    return data_gen, names_gen


def sample_refit(analysis, family, num_clusters, n_sample,
                 stdout=True, plot=False, save=True, save_gen_data=False,
                 filename=None, out_path=None):

    x_name = analysis.rx2('feature_x')[0]
    y_name = analysis.rx2('feature_y')[0]

    if filename is None:
        filename = f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}_gen'
    if not os.path.isdir(out_path):
        os.makedirs(out_path)

    data_gen, names_gen = sample(analysis, n_sample, x_name=x_name, y_name=y_name)

    if save_gen_data:
        data_gen.to_csv(filename)

    analysis_gen = fit(data_gen, family, num_clusters,
                       stdout=stdout, plot=plot, save=save,
                       filename=filename, out_path=out_path)

    return analysis_gen
