from dataclasses import dataclass
from pathlib import Path

import rpy2.robjects as robjects

from rpy2.robjects.packages import importr
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage


# configuration
MIXSMSN_PATH = ''  # if you need to set path manually
CLUST_GEN_PATH = ''  # if you need to set path manually

PLOT_DIR = Path('plots')
TABLE_DIR = Path('tables')
ANALYSES_DIR = Path('analyses')
ANALYSES_DIR_BINARY = ANALYSES_DIR / 'binary'

PROBS_DEFAULT = [0.68, 0.997]  # [0.997, 0.95, 0.68]

# for get_z.R
AX_LIMITS_DEFAULT = {
    "x.left": -5.0,
    "x.right": 5.0,
    "y.bottom": -5.0,
    "y.top": 5.0
}

# for get_z.R
PADDING_DEFAULT = {
    "x.pad_left": 1.0,
    "x.pad_right": 1.0,
    "y.pad_bottom": 1.0,
    "y.pad_top": 1.0
}

# for get_z.R
PADDING_ZERO = {
    "x.pad_left": 0.0,
    "x.pad_right": 0.0,
    "y.pad_bottom": 0.0,
    "y.pad_top": 0.0
}

COLORS = ['seagreen', 'tomato', 'mediumpurple', 'dodgerblue', 'darkgoldenrod']
# end of configuration


# import an R script
def get_r_package(path_to_r_package, python_package_name):
    with open(path_to_r_package, 'r') as f:
        r = f.readlines()
    package = SignatureTranslatedAnonymousPackage('\n'.join(r), python_package_name)
    return package


if MIXSMSN_PATH:
    mixsmsn = importr('mixsmsn', lib_loc=MIXSMSN_PATH)
else:
    mixsmsn = importr('mixsmsn')

if CLUST_GEN_PATH:
    genPositiveDefMat = importr('clusterGeneration', lib_loc=CLUST_GEN_PATH).genPositiveDefMat
else:
    genPositiveDefMat = importr('clusterGeneration').genPositiveDefMat

c_r = robjects.r('c')
list_r = robjects.r('list')
print_r = robjects.r('print')
matrix_r = robjects.r('matrix')
length_r = robjects.r('length')
typeof_r = robjects.r('typeof')

get_z = get_r_package('get_z.R', 'get_z')
pdfs = get_r_package('pdfs.R', 'pdfs')
make_analysis = get_r_package("./make_analysis.R", "make_analysis")

if not PLOT_DIR.is_dir():
    Path.mkdir(PLOT_DIR)

if not TABLE_DIR.is_dir():
    Path.mkdir(TABLE_DIR)

if not ANALYSES_DIR.is_dir():
    Path.mkdir(ANALYSES_DIR)

if not ANALYSES_DIR.is_dir():
    Path.mkdir(ANALYSES_DIR_BINARY)

FAMILIES = ['Normal', 'Skew.normal', 't', 'Skew.t']
FAMILIES_NOT_SUPPORTED = ['Skew.cn', 'Skew.slash']  # these can be fit, cannot be sampled
FAMILIES_ALL = FAMILIES + FAMILIES_NOT_SUPPORTED

PDF_BY_FAMILY = {
    'Normal': pdfs.N,
    'Skew.normal': pdfs.SN,
    't': pdfs.T,
    'Skew.t': pdfs.ST,
}

PDF_MIXTURE_BY_FAMILY = {
    'Normal': pdfs.mixed_N,
    'Skew.normal': pdfs.mixed_SN,
    't': pdfs.mixed_T,
    'Skew.t': pdfs.mixed_ST,
}

SHORT_NAME_BY_FAMILY = {
    'Normal': 'N',
    'Skew.normal': 'SN',
    't': 'T',
    'Skew.t': 'ST',
}
