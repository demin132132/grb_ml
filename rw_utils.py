import os
import pickle


def pickle_obj(obj, filename, path):
    if not filename.endswith('.pickle'):
        filename = f'{filename}.pickle'
    with open(os.path.join(path, filename), 'wb') as wf:
        pickle.dump(obj, file=wf, protocol=pickle.DEFAULT_PROTOCOL)


def read_pickled(filename, path):
    if not filename.endswith('.pickle'):
        filename = f'{filename}.pickle'
    with open(os.path.join(path, filename), 'rb') as rf:
        obj = pickle.load(rf)
    return obj


def write(lines, filename, path):
    """
    :param filename: str
        Requires file extension.
    :param lines: list[str]
    :param path: path-like
    :return:
    """
    full_path = os.path.join(path, filename)
    with open(full_path, 'w') as wf:
        wf.write("\n".join(lines))


def read_analyses(path):

    if not os.path.isdir(path):
        raise FileNotFoundError(f"Path {path} doesn't exist.")

    analysis_filenames = [
        fname
        for fname in os.listdir(path)
        if fname.endswith('.pickle')
    ]
    analyses = [read_pickled(fname, path=path) for fname in analysis_filenames]

    return analyses
