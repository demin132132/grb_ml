`Outdated`: Для работы необходимо установить R, библиотеку `rpy2`, а также запустить `install_r_packages.py` либо вручную установить пакет `mixsmsn` в R.  

Чтобы строить иные зависимости, достаточно скачать таблицу с нужными данными и дополнить `FEATURES_X` / `FEATURES_Y` / `FEATURES_Y_NOT_LOG` в main.py.


## `fit.py`

Functions for fitting data.


### `fit(data_r, feature_x, feature_y, family, num_clusters)`

Fits `data_r` data with `num_clusters` clusters on the `feature_x`&ndash;`feature_y` plane.  
Probability distribution family is determined by the `family` parameter.  
Calls `mixsmsn.smsn_mmix`.  
Upon creation of "analysis" object, immediately creates fields `family`, `num_clusters`, `feature_x` and `feature_y` in it.


### `fit_stdout(analysis, feature_x, feature_y, family, num_clusters)`

Function for real-time printing to stdout while fitting.


### `fit_plot(data, analysis, feature_x, feature_y, family, num_clusters, data_r=None, ax_limits=None, padding=None, gen=False)`

Function for creating and saving plots.


### `fit_save(analyses, feature_x, feature_y, numbers_clusters, families, filename=None, gen=False)`

Function for saving a `list` of "analysis" objects as to file.  
Converts `list` to `pd.DataFrame` before writing to file.


### `fit_one(data, data_r, feature_x, feature_y, family, num_clusters, stdout=True, plot=False, save=True, filename=None, gen=False, ax_limits=None, padding=None)`

Fits data with one set of parameters passed as arguments. Returns "analysis" object. Optional: saves fit results as a binary file, saves plot and prints to stdout.  
This is a lower-level option for fitting data with one `family` and one `num_clusters`. `fit_one` is called inside `fit_all` and `fit_single`. If you want to fit data a single time, consider using `fit_single` and fall back to `fit_one` if you need a more custom option (for example, configure all parameters outside the function).

Prints to stdout if `stdout` is `True`.  
Saves plot if `plot` is `True`.  
Saves "analysis" object as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.

For parameter description, see `fit_single`.


### `fit_single(stdout=True, plot=False, save=True, filename=None)`

Fits data with one set of configurable parameters. Returns "analysis" object.  
All configuring is done inside the function. Lines that you might want to change are marked with the `# customize` comment:  

- `feature_x`  
   X-axis feature.
- `feature_y`  
   Y-axis feature.
- `family`  
   Probability distribution family. See `FAMILIES` and `FAMILIES_NOT_SUPPORTED` in `base.py` for more info.
- `num_clusters`  
   Number of clusters.
- `ax_limits`  
   `dict` with axes limits for plotting.
- `padding`  
   `dict` with axes padding for plotting.
  
X-axis and Y-axis features must be present in the corresponding dictionaries `FEATURES_X` and `FEATURES_Y` in `base.py`.

Prints to stdout if `stdout` is `True`.  
Saves plot if `plot` is `True`.  
Saves "analysis" object as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.


### `fit_all(stdout=True, plot=False, save=True, filename=None)`

Fits data multiple times with many combinations of parameters according to configuration.  
All configuring is done inside the function. Lines that you might want to change are marked with the `# customize` comment:

- `running_features_x`  
   `list` of X-axis features.
- `running_features_y`  
   `list` of Y-axis features.

X-axis and Y-axis features must be present in the corresponding dictionaries `FEATURES_X` and `FEATURES_Y` in `base.py`.

- `running_families`  
   `list` of families.
- `running_numbers_clusters`  
   `list` of numbers of clusters.

All combinations of the above parameters are considered.

- `ax_limits`  
   `dict` with axes limits for plotting.
- `padding`  
   `dict` with axes padding for plotting.

Prints to stdout while fitting if `stdout` is `True`.  
Saves plots if `plot` is `True`.  
Saves one `pd.DataFrame` of "analysis" objects *per plane* (`feature_x`&ndash;`feature_y`) as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.


### `main()`

Contains how-to examples of fitting data:
- either with one combination of parameters (`fit_single`) or with a range of parameters at once (`fit_all`; need to save "analyses" `pandas` table(s) to file and then read from it).



## `sample.py`


### `sample_ds(analysis, n_sample)`

Sample `n_sample` points from mixture (parameters given in `analysis`) using `numpy.random.multivariate_normal`.  
Only supports `"Normal"` probability distribution family.


### `sample(analysis, pdf, z, x_min, x_max, y_min, y_max, n_sample)`

Sample `n_sample` points from mixture (family, number of clusters and distribution parameters are given in `analysis`) using rejection sampling method using uniform distribution scaled by a constant factor as a "ceiling".

`pdf`:  probability density function for the mixture. See `PDF_MIXTURE_BY_FAMILY` in `base.py`.  
`z`: mixture PDF numerically computed on a grid of values.  
`x_min`, `x_max`, `y_min`, `y_max`: X and Y limits for rejection sampling.

Not meant to be called directly &mdash; `sample` is called inside `generate_data`.


### `generate_data(analysis, family, feature_x, feature_y, n_sample, ax_limits=None, padding=None)`

Generates data using `sample` function (see `sample` in `sample.py`).  
Returns `pd.DataFrame` with the generated data points which also contains the `name` column that acts as a point ID. Point's `name` is its number with leading zeros and prefix `GEN`.


### `sample_one(analysis, feature_x, feature_y, family, num_clusters, n_sample, stdout=True, plot=False, save=True, filename=None, ax_limits=None, padding=None)`

Generates data, fits it and returns the "analysis" object. Meant for use with one set of parameters passed as arguments. Optional: saves fit results as a binary file, saves plot and prints to stdout.  
This is a lower-level option for sampling/fitting data with one `family` and one `num_clusters`. `sample_one` is called inside `sample_all` and `sample_single`. If you want to sample/fit data a single time, consider using `sample_single` and fall back to `sample_one` if you need a more custom option (for example, configure all parameters outside the function).

Prints to stdout if `stdout` is `True`.  
Saves plot if `plot` is `True`.  
Saves "analysis" object as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.

For parameter description, see `sample_single`.


### `sample_single(analysis, stdout=True, plot=True, save=True, filename=None)`

Generates data, fits it and returns "analysis" object. Mean for use with one set of configurable parameters.  
All configuring is done inside the function. Lines that you might want to change are marked with the `# customize` comment:  

- `feature_x`  
   X-axis feature.
- `feature_y`  
   Y-axis feature.
- `n_sample`  
   Number of points to be sampled.
- `ax_limits`  
   `dict` with axes limits for plotting.
- `padding`  
   `dict` with axes padding for plotting.

X-axis and Y-axis features must be present in the corresponding dictionaries `FEATURES_X` and `FEATURES_Y` in `base.py`.  
Probability distribution family and number of clusters are picked up from the `analysis` argument.

Prints to stdout if `stdout` is `True`.  
Saves plot if `plot` is `True`.  
Saves "analysis" object as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.



### `sample_all(analyses, stdout=True, plot=False, save=True, filename=None)`

Generates data and fits it multiple times with many combinations of parameters according to the input `analyses` argument.  
All configuring is done inside the function. Lines that you might want to change are marked with the `# customize` comment:

- `running_features_x`  
   `list` of X-axis features.
- `running_features_y`  
   `list` of Y-axis features.

X-axis and Y-axis features must be present in the corresponding dictionaries `FEATURES_X` and `FEATURES_Y` in `base.py`.

- `running_families`  
   `list` of families.
- `running_numbers_clusters`  
   `list` of numbers of clusters.

All combinations of the above parameters are considered.

- `ax_limits`  
   `dict` with axes limits for plotting.
- `padding`  
   `dict` with axes padding for plotting.

Prints to stdout while fitting if `stdout` is `True`.  
Saves plots if `plot` is `True`.  
Saves one `pd.DataFrame` of "analysis" objects *per plane* (`feature_x`&ndash;`feature_y`) as a binary file if `save` is `True`. One can specify `filename` for this file. To change path where the "analysis" file is put, change the `ANALYSIS_DIR_BINARY` constant in `base.py`.



### `main()`

...



## `base.py`

Classes, functions and variables that are used project-wide.  
Meant to be imported in other scripts.


### `Feature`

Dataclass for gamma-ray burst features with fields:  
- `table_name: str`  
   feature name in the data table
- `name: str`  
   human readable feature name
- `unit: str`  
   feature unit of measurement
- `log: bool`  
   True if feature data should be log


### `FEATURES_X: dict`

Dictionary of features for X axis.  
Contains a generic feature `x_feature` for when the `analysis` object is being generated and thus the feature has no underlying physical meaning.


### `FEATURES_Y: dict`

Dictionary of features for Y axis.


### `get_r_package(path_to_r_package, python_package_name)`

Function for importing R packages/scripts into Python.  
Imports from full path `path_to_r_package` and names it `python_package_name` in Python.


### `mixsmsn`

R `mixsmsn` package imported into Python.


### `c_r`

R `c()` function imported into Python.


### `get_z`

R `get_z.R` script imported into Python. See `get_z.R` for more info.


### `pdfs`

R `pdfs.R` script imported into Python. See `pdfs.R` for more info.


### `make_analysis`

R `make_analysis.R` script imported into Python. See `make_analysis.R` for more info.


### `AX_LIMITS_DEFAULT: dict`

Default axes limits for plots. See `get_z.R` for more info.

### `PADDING_DEFAULT: dict`

Default padding for plots. See `get_z.R` for more info.


### `COLORS: list`

A list of colors for plots. 1 color per cluster.


### `INPUT_DIR: path-like`

Directory containing input data table.


### `PLOT_DIR: path-like`

Directory where plots are saved to.


### `ANALYSES_DIR: path-like`

Directory where "analysis" markdown files are saved to.


### `ANALYSES_DIR_BINARY: path-like`

Directory where "analysis" binary files are saved to.


### `FAMILIES: list`

List of probability distribution families that are supported for fitting and sampling.


### `FAMILIES_NOT_SUPPORTED: list`

List of probability distribution families that are supported for fitting (but not yet for sampling).


### `FAMILIES_ALL: list`

List of all probability distribution families.


### `DISTRIBUTIONS_INFO_FILE`

Outdated. To be removed.


### `PDF_MIXTURE_BY_FAMILY: dict`

Dictionary that maps probability distribution family names to corresponding probability distribution functions imported from `pdfs.R` script.


### `SHORT_NAME_BY_FAMILY: dict`

Dictionary that maps probability distribution family names to their short versions.



## `conv_utils.py`

Converter functions.


### `converted_to_r(df_py)`

Converts Python `pandas.DataFrame` to R `data.frame`.


### `converted_to_pandas(data_r)`

Converts R `data.frame` to Python `pandas.DataFrame`.


### `analyses_to_pandas(analyses, numbers_clusters, families)`

Converts `list` of "analysis" objects to `pandas.DataFrame` of "analysis" objects.  
Also requires `families: list` and `numbers_clusters: list` that were used for fitting.


## `data_utils.py`

Functions for reading and processing data.


### `get_data(filename, path=INPUT_DIR, feature_x_names=None, feature_y_names=None, sep=r' *\|')`

Read data from `filename` located in `path` and processes it:
- delete rows containing NA;
- log `feature_x_names` and `feature_x_names` data if needed (if the arguments are supplied).

Returns `pandas.DataFrame`. *No columns of the input table are omitted.*



## `out_utils.py`

Functions for output.


### `gen_md_text_one(analysis, heading=None)`

Generates contents of markdown file for one "analysis" object.


### `gen_md_text_all(analyses, heading=None)`

Generates contents of markdown file for a list of "analysis" objects.


### `gen_md(anls, filename, heading=None, path=ANALYSES_DIR)`

Writes markdown file `filename` corresponding to `anls` and puts it in `path`.  
`anls` may be either an "analysis" object or a list of those.  
Convenient higher level of abstraction function combining functionality of the two functions above that also does the writing.



## `plot_utils.py`

Functions for plotting.


### `plot_fit(data, x_grid, y_grid, z, family, feature_x, feature_y, title, filename, path=PLOT_DIR, x_lim=None, y_lim=None)`

Plot the points from `data: pandas.DataFrame` on the `feature_x`&ndash;`feature_y` plane.  
`x_lim` and `y_lim` are array-like of 2 elements that give left and right limits for corresponding axes. If not supplied, limits are calculated from `x_grid` and `y_grid`.
Plots contours according to the `z` parameter that gives resulting probability density function values calculated on the grid. `x_grid`, `y_grid` and `z` are meant to be obtained by calling `get_z` function from the `get_z.R` R script.  
Saves the plot with name `filename` and puts it in `path`.


### `confidence_ellipse(center, cov, ax, n_std=3.0, facecolor='none', **kwargs)`

\[in development\] Plot a `n_std` sigma confidence ellipse onto `ax` axis centered in `center` (x and y coordinates).


### `def plot_dens_3d(x_grid, y_grid, z)`

Plot probability density function on the grid in 3d.  
`x_grid`, `y_grid` and `z` are meant to be obtained by calling `get_z` function from the `get_z.R` R script.  
`z` is a 2D array that gives pdf height at every point of the grid.


### `plot_dens_heatmap(x_grid, y_grid, z, analysis, plot_centers=True, plot_ellipses=False)`

Plot probability density function on the grid as a 2d heatmap.  
`x_grid`, `y_grid` and `z` are meant to be obtained by calling `get_z` function from the `get_z.R` R script.  
`z` is a 2D array that gives pdf height at every point of the grid.  
Plots centers if `plot_centers`. Plots confidence ellipses if `plot_ellipses`.


### `launch_plot(analysis, ax_limits=None, padding=None, plot_type="heatmap")`

Plot a `plot_type` plot (either `"heatmap"` or `"3d"`) corresponding to the `analysis` object.  
One can specify `ax_limits` and `padding` for `get_z` (see `get_z.R` for more info). If not supplied, falls back to default values specified in `base.py`.
Convenient higher level of abstraction function combining functionality of the two functions above.



## `rw_utils.py`

Functions for reading/writing files.


### `pickle_obj(obj, filename, path)`

Pickles object `obj` into file `filename` and puts it in `path`.


### `read_pickled(filename, path)`

Read pickled object from `filename` located in `path`.
Returns the unpickled object.


### `write(lines, filename, path)`

Writes `lines: list[str]` to file `filename` and puts it in `path`.



## `utils.py`

Other functions.


### `get_analysis()`

Returns "analysis" object created with `make_analysis.R` script.


### `get_grid_dens(analysis, data_r=None, ax_limits=None, padding=None)`

Calculates probability density function on a grid.  
Axes limits are obtained from data if `data_r` (R `data.frame`) is supplied.  
One can specify `ax_limits` and `padding` for `get_z` (see `get_z.R` for more info). If not supplied, falls back to default values specified in `base.py`.  
Returns `x_grid`, `y_grid`, `z` (`tuple` of 3 elements).  
A convenient wrapper function for `get_z` from `get_z.R` script.


### `prob_by_cluster(analysis, x, y, family=None, num_clusters=None)`

Calculates probability that point (x, y) belongs to each cluster from `analysis` object.
Returns an array of values (sums to 1) with length = `num_clusters`.



## `fit_points.R`

An example of fitting data in R (without Python) and accessing results.



## `get_z.R`

Script with the `get_z` function.


### `get_z(data, model, num_pts_grid = 100, x.left, x.right, y.bottom, y.top, x.pad_left=1.0, x.pad_right=1.0, y.pad_bottom=1.0, y.pad_top=1.0)`

Calculates resulting probability density function values on a grid.  
Grid fineness is determined by `num_pts_grid` argument. A value of 100 means there are 100^2 = 10'000 points total.  
If axes limits are not provided explicitly as parameters, they will be determined from data.  
`model` parameter is the "analysis" object.  
One can specify padding.  
Returns `x_grid`, `y_grid`, `z`.


## `make_analysis.R`

R script for creating custom "analysis" objects.


### `analysis_`

Auxiliary function that actually does the "analysis" object construction.  
Called by make_analysis function. Not meant to be called directly.


### `make_analysis`

Function to be called to create the "analysis" object inside R.  
To create "analysis" object inside Python, use `get_analysis` function from `utils.py`.  
Probability distribution parameters are set inside the function.  



## `pdfs.R`

Probability density functions.



## `install_r_packages.py`

...



## `test.py`

...



## `dens.R`

No longer used.


