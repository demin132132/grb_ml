import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import AutoMinorLocator

import plotly as py
import plotly.graph_objs as go
from plotly.subplots import make_subplots

from base import *
from conv_utils import converted_to_pandas
from utils import get_grid_dens, split_analysis, probs_to_levels
from rw_utils import read_analyses

# mpl.rcParams['font.sans-serif'] = 'Computer Modern Sans Serif'
# mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams["axes.autolimit_mode"] = 'round_numbers'
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'


# построение точек и контуров
def plot_html(data, analysis, names=None, filename=None, out_path=None, probs=None,
              x_limits=None, y_limits=None, title=None, x_label=None, y_label=None,
              ax_limits=None):
    """
    :param ax_limits: dict
    :param y_label: str
    :param x_label: str
    :param names: 1D array-like
    :param data: pd.DataFrame
    :param analysis: rpy2.robjects.vectors.ListVector
    :param title: str
    :param filename: str
    :param out_path: path-like
    :param probs: array-like (1D)
        array of integral probabilities for contours
    :param x_limits: array-like (1D)
        Should contain two elements: min and max values for plotting (X axis).
    :param y_limits: array-like (1D)
        Should contain two elements: min and max values for plotting (Y axis).
    :return: None
    """

    x_col_name, y_col_name = data.columns
    family = analysis.rx2('family')[0]
    num_clusters = analysis.rx2('num_clusters')[0]

    if probs is None:
        probs = PROBS_DEFAULT

    if names is None:
        names = list(range(len(data)))
    if not isinstance(names, pd.DataFrame):
        names = pd.DataFrame(names)

    if x_label is None:
        x_label = x_col_name
    if y_label is None:
        y_label = y_col_name
    if title is None:
        title = f'{x_col_name} vs {y_col_name}, {num_clusters}{SHORT_NAME_BY_FAMILY[family]}'

    if filename is None:
        filename = f'{x_col_name}_{y_col_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}'
    if not filename.endswith('.html'):
        filename = f'{filename}.html'

    if out_path is None:
        out_path = os.path.join(PLOT_DIR, f'{x_col_name}_{y_col_name}')
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    fig = make_subplots(specs=[[{'secondary_y': True}]])

    model_group_r = analysis.rx2('group')
    model_group = converted_to_pandas(model_group_r)

    clusters = data.groupby(model_group)
    clusters_names = names.groupby(model_group)

    for i, group_key in enumerate(clusters.groups):
        cluster = clusters.get_group(group_key)
        cluster_names = clusters_names.get_group(group_key)

        fig.add_trace(
            go.Scatter(
                x=cluster[x_col_name],
                y=cluster[y_col_name],
                mode='markers',
                marker={
                    'size': 4,
                    'color': COLORS[i],
                },
                name=f'cluster {i + 1}',
                text=cluster_names
            ),
            secondary_y=False
        )

    for i in range(num_clusters):
        sub_analyses = split_analysis(analysis)

        x_grid, y_grid, z = get_grid_dens(sub_analyses[i], ax_limits=ax_limits)
        z = z / z.sum()
        levels = probs_to_levels(z, probs=probs, n=1000)

        for j in range(len(levels)):
            fig.add_trace(
                go.Contour(
                    z=z,
                    x=x_grid,
                    y=y_grid,
                    colorscale=[COLORS[i], COLORS[i]],
                    hoverinfo='skip',
                    autocontour=False,
                    contours={
                        'showlabels': True,
                        'type': 'constraint',
                        'value': levels[j],
                        'operation': '=',
                        'labelfont': {
                            'size': 12,
                            'color': 'blue'
                        },
                    },
                    name=f"{family} {probs[j]}",
                    showscale=False,
                ),
                secondary_y=True,
            )

    fig.update_layout(
        title=title,
        yaxis={
            'title': x_label,
            'range': [min(y_grid), max(y_grid)] if y_limits is None else y_limits
        },
        xaxis={
            'title': y_label,
            'range': [min(x_grid), max(x_grid)] if x_limits is None else x_limits
        },
    )

    py.offline.plot(fig, filename=os.path.join(out_path, filename), auto_open=False)


def plot_dens_3d(x_grid, y_grid, z):
    """
    :param x_grid: array-like (1D)
    :param y_grid: array-like (1D)
    :param z: array-like (2D)
    :return:
    """
    x, y = np.meshgrid(x_grid, y_grid)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(x, y, z, cmap="plasma")
    return fig, ax


def plot_dens_heatmap(x_grid, y_grid, z, analysis, plot_centers=True):
    """
    :param x_grid: array-like (1D)
    :param y_grid: array-like (1D)
    :param z: array-like (2D)
    :param analysis: rpy2.robjects.vectors.ListVector
    :param plot_centers: bool
    :paramplot_ellipses: bool
    :return: tuple(matplotlib.figure.Figure, matplotlib.axes.Axes)
    """
    # ellipse centers have a significant offset for Skew.Normal
    x, y = np.meshgrid(x_grid, y_grid)
    z_min, z_max = z.min(), z.max()

    fig, ax = plt.subplots()

    c = ax.pcolormesh(x, y, z, cmap='plasma', vmin=z_min, vmax=z_max)
    ax.axis([x.min(), x.max(), y.min(), y.max()])  # set the limits of the plot to the limits of the data
    fig.colorbar(c, ax=ax)

    colors = ["red", "green", "blue"]

    for i in range(analysis.rx2("num_clusters")[0]):
        mu = np.asarray(analysis.rx2("mu")[i])
        if plot_centers:
            ax.plot(*mu, color=colors[i], marker='o', markersize=3)

    return fig, ax


def launch_plot(analysis, plot_type="heatmap", ax_limits=None, padding=None):
    """
    :param analysis: rpy2.robjects.vectors.ListVector
    :param plot_type: str
    :param ax_limits: dict
        see AX_LIMITS_DEFAULT var in base.py for reference.
    :param padding: dict
    :return:
    """

    if ax_limits is None:
        ax_limits = AX_LIMITS_DEFAULT

    if padding is None:
        padding = PADDING_DEFAULT

    assert plot_type in ["heatmap", "3d"], "'plot' argument not recognized."

    x, y, z = get_grid_dens(analysis=analysis, data_r=None, ax_limits=ax_limits, padding=padding)
    x = np.asarray(x)
    y = np.asarray(y)
    z = np.asarray(z).T

    if plot_type == "heatmap":
        plot_dens_heatmap(x, y, z, analysis, plot_centers=False)
    elif plot_type == "3d":
        plot_dens_3d(x, y, z)
    else:
        raise ValueError("Unknown 'plot' argument.")

    plt.show()


def plot_criteria(analyses, x_name=None, y_name=None, title=None, save=False, ext='png', filename=None, out_path=None):

    if x_name is None:
        x_name = 'feature_x'
    if y_name is None:
        y_name = 'feature_y'

    if out_path is None:
        out_path = PLOT_DIR
    if not os.path.isdir(out_path):
        os.makedirs(out_path)

    if filename is None:
        filename = f'{x_name}_{y_name}_criteria.{ext}'

    if title is None:
        title = f'{x_name} vs {y_name}'

    models = []
    bics = []
    aics = []
    logliks = []

    for analysis in analyses:
        num_clusters = analysis.rx2('num_clusters')[0]
        family = analysis.rx2('family')[0]
        model = f"{num_clusters}{SHORT_NAME_BY_FAMILY[family]}"

        bic = analysis.rx2('bic')[0]
        aic = analysis.rx2('aic')[0]
        loglik = analysis.rx2('logLik')[0]

        models.append(model)
        bics.append(bic)
        aics.append(aic)
        logliks.append(loglik)

    models = np.array(models)
    bics = np.array(bics)
    aics = np.array(aics)
    logliks = np.array(logliks)

    bic_min = np.min(bics)
    aic_min = np.min(aics)
    loglik_min = np.min(logliks)

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    x = list(range(len(analyses)))
    plt.xticks(x, models)
    bic_line = ax1.plot(x, bics - bic_min, 'o-', color='b', alpha=.6, label=f'BIC (BIC_min = {bic_min:.0f})')
    aic_line = ax1.plot(x, aics - aic_min, 'o-', color='r', alpha=.6, label=f'AIC (AIC_min = {aic_min:.0f})')
    loglik_line = ax2.plot(x, logliks - loglik_min, 'o-', color='k', alpha=.6, label=f'LL (LL_min = {loglik_min:.0f})')

    lines = bic_line + aic_line + loglik_line
    labels = [line.get_label() for line in lines]
    plt.legend(lines, labels, loc=0)

    ax1.yaxis.set_minor_locator(AutoMinorLocator(5))
    ax2.yaxis.set_minor_locator(AutoMinorLocator(5))

    ax1.set_axisbelow(True)
    ax2.set_axisbelow(True)

    ax1.yaxis.grid(which='major', color='#e1e1e1')
    ax1.yaxis.grid(which='minor', color='#f8f8f8')

    ax1.set_ylabel(r'$\Delta$AIC, $\Delta$BIC')
    ax2.set_ylabel(r'$\Delta$LL')

    fig.suptitle(title)

    if save:
        plt.savefig(os.path.join(out_path, filename), dpi=300)
    else:
        plt.show()

    return fig, ax1, ax2


def plot_fit(data, analysis, filename=None, out_path=None, probs=None, save=True,
             x_limits=None, y_limits=None, x_label=None, y_label=None, title=None,
             ax_limits=None):

    x_col_name, y_col_name = data.columns
    family = analysis.rx2('family')[0]
    num_clusters = analysis.rx2('num_clusters')[0]

    if probs is None:
        probs = PROBS_DEFAULT
    if filename is None:
        filename = f'{x_col_name}_{y_col_name}'
    filename, ext = os.path.splitext(filename)
    if out_path is None:
        out_path = os.path.join(PLOT_DIR, f'{x_col_name}_{y_col_name}')
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    if title is None:
        title = f'{x_col_name} vs {y_col_name}, {num_clusters}{SHORT_NAME_BY_FAMILY[family]}'

    cluster_colors = ['r', 'b', 'g']  # add more colors for more clusters
    contour_colors = ['tomato', 'deepskyblue', 'limegreen']  # add more colors for more clusters

    model_group_r = analysis.rx2('group')
    model_group = converted_to_pandas(model_group_r)

    probs_fmt = [f'{round(p * 100, 1)}%' for p in probs]
    probs_fmt = list(reversed(probs_fmt))

    fig, ax = plt.subplots(figsize=(4.8, 4.8))
    # fig, axes = plt.subplots(nrows=1, ncols=len(sub_analyses), figsize=(24, 8))  # , sharey=True, sharex=True)

    clusters = data.groupby(model_group)
    for i, group_key in enumerate(clusters.groups):
        cluster = clusters.get_group(group_key)
        ax.scatter(x=cluster[x_col_name], y=cluster[y_col_name],
                   s=.1, color=cluster_colors[i], alpha=.8)

    if x_limits is not None:
        x_left, x_right = x_limits
        ax.set_xlim(left=x_left, right=x_right)

    if y_limits is not None:
        y_bottom, y_top = y_limits
        ax.set_ylim(bottom=y_bottom, top=y_top)

    sub_analyses = split_analysis(analysis)
    # sub_analyses.append(analysis)

    for i in range(len(sub_analyses)):
        x_grid, y_grid, z = get_grid_dens(sub_analyses[i], ax_limits=ax_limits)
        z = z / z.sum()
        levels = probs_to_levels(z, probs=probs)
        levels = sorted(levels.tolist())
        cs = ax.contour(x_grid, y_grid, z, levels, colors=contour_colors[i], linestyles=('solid', 'dashed'),
                        linewidths=.7)
        fmt = {}
        for level, string in zip(cs.levels, probs_fmt):
            fmt[level] = string
        ax.clabel(cs, cs.levels, fontsize=6, inline=True, fmt=fmt)
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))
    ax.set_axisbelow(True)
    ax.grid(which='major', color='#e1e1e1', linewidth=.5)
    ax.grid(which='minor', color='#f8f8f8', linewidth=.5)

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title, y=1.03)

    if save:
        plt.savefig(os.path.join(out_path, filename), dpi=300, bbox_inches='tight')
    else:
        plt.show()

    return fig, ax
