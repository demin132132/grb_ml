import os
import math
import random
import itertools

import numpy as np
from tqdm import tqdm

from base import *
from conv_utils import converted_to_r
from plot import plot_html
from rw_utils import pickle_obj, read_pickled


def _fit(data, family, num_clusters, **kwargs):

    x_col_name, y_col_name = data.columns
    data_r = converted_to_r(data)

    if all(arg in kwargs for arg in ['mu', 'sigma', 'shape', 'pii', 'nu']):
        # print("Fitting with the provided initial parameters...")
        analysis = mixsmsn.smsn_mmix(
            y=data_r,  # XY table, R data type
            g=num_clusters,  # number of clusters
            family=family,  # distribution family
            group=True,  # if TRUE, the vector with the classification of the response is returned
            calc_im=False,  # if TRUE, the information matrix is calculated and the standard errors are reported
            get_init=False,  # if TRUE, the initial values are generated via k-means
            mu=kwargs['mu'],  # following are parameter initial values
            Sigma=kwargs['sigma'],
            shape=kwargs['shape'],
            pii=kwargs['pii'],
            nu=kwargs['nu']
        )
    else:
        # print("At least one initial parameter value is not specified. They will be determined from k-means")
        analysis = mixsmsn.smsn_mmix(
            y=data_r,
            g=num_clusters,
            family=family,
            group=True,
            calc_im=False,
            get_init=True
        )

    # accounts for mixsmsn authors putting sqrt(sigma) instead of sigma in the analysis ListVector - we just square it
    # reference: https://cran.r-project.org/web/packages/mixsmsn/mixsmsn.pdf; see page 15 (smsn.mmix), section "Value"
    for i in range(len(analysis.rx2['Sigma'])):
        analysis.rx2['Sigma'][i] = pdfs.squared(analysis.rx2['Sigma'][i])

    analysis.rx2["family"] = family
    analysis.rx2["num_clusters"] = num_clusters
    analysis.rx2["feature_x"] = x_col_name
    analysis.rx2["feature_y"] = y_col_name

    return analysis


def fit_stdout(analysis, x_name, y_name, family, num_clusters):
    print(f'{x_name} vs {y_name}, {f"{num_clusters}{SHORT_NAME_BY_FAMILY[family]}":>3}: '
          f'bic = {round(analysis.rx2("bic")[0], 2):.2f}, '
          f'logLik = {round(analysis.rx2("logLik")[0], 2):.2f}')


def fit_save(analysis, x_name, y_name, num_clusters, family, filename=None, path=None):
    if filename is None:
        filename = f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}'
    if path is None:
        path = os.path.join(ANALYSES_DIR_BINARY, f'{x_name}_{y_name}')
    if not os.path.exists(path):
        os.makedirs(path)
    pickle_obj(analysis, filename=filename, path=path)


def fit(data, family, num_clusters,
        stdout=True, plot=False, save=False, filename=None,
        out_path=None, **kwargs):
    """
    :param data: pd.DataFrame
    :param family: str
    :param num_clusters: int
    :param stdout: bool
    :param plot: bool
    :param save: bool
    :param filename: str
    :param out_path: path-like
    :return:
    """

    analysis = _fit(data, family, num_clusters, **kwargs)
    x_name, y_name = data.columns

    if stdout:
        fit_stdout(analysis, x_name, y_name, family, num_clusters)

    if plot:
        plot_html(data, analysis)

    if save:
        fit_save(analysis, x_name, y_name, num_clusters, family,
                 filename=filename, path=out_path)

    return analysis


def fit_many(data, nums_clusters, families, stdout=True, plot=False):

    analyses = []

    for num_clusters in nums_clusters:

        for family in families:

            analysis = fit(data, family, num_clusters,
                           stdout=stdout, plot=plot, save_analysis=True)
            analyses.append(analysis)

    return analyses


def random_fit(data, nums_clusters, families, num_runs=1000):
    x_name, y_name = data.columns
    for num_clusters in nums_clusters:
        for family in families:
            shape = list_r(*tuple(c_r(*s) for s in [[0, 0]] * num_clusters))
            pii = c_r(0.334, 0.333, 0.333) if num_clusters == 3 else c_r(0.5, 0.5)

            init_params = {
                'mu': None,
                'sigma': None,
                'shape': shape,
                'pii': pii,
                'nu': 1
            }

            for i in range(num_runs):

                mus = ...

                cov = genPositiveDefMat(
                    dim=2,
                    covMethod='eigen',
                    rangeVar=c_r(0.1, 7)
                )

                init_params['mu'] = list_r(*tuple(c_r(*mu.tolist()) for mu in mus))
                init_params['sigma'] = cov

                filename = f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}_random_{i}'
                fit(data, family, num_clusters,
                    stdout=False, plot=False, save=False,
                    filename=filename, **init_params)


def centers_grid_search(data, nums_clusters, families, random_cov=False,
                        grid_shape=(3, 3), width=.3, height=.3, ratio=True, around_centers=True):
    if around_centers is False:
        raise NotImplementedError
    if ratio is False:
        raise NotImplementedError
    assert len(grid_shape) == 2, 'Invalid "grid_shape" argument.'

    x_name, y_name = data.columns
    w = float(data[x_name].max()) - float(data[x_name].min())
    h = float(data[y_name].max()) - float(data[y_name].min())

    for num_clusters in nums_clusters:

        for family in families:

            sigma = list_r(*tuple(matrix_r(c_r(*s), nrow=2) for s in [[1., 0., 0., 1.]] * num_clusters))
            shape = list_r(*tuple(c_r(*s) for s in [[0, 0]] * num_clusters))
            pii = c_r(0.334, 0.333, 0.333) if num_clusters == 3 else c_r(0.5, 0.5)

            init_params = {
                'mu': None,
                'sigma': sigma,
                'shape': shape,
                'pii': pii,
                'nu': 1
            }

            analysis = read_pickled(
                filename=f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}',
                path=os.path.join(ANALYSES_DIR_BINARY, f'{x_name}_{y_name}'))
            grid_centers = np.array(analysis.rx2['mu'])

            mu_grids = []
            for grid_center in grid_centers:
                x_left, y_bottom = grid_center - np.array((w * width / 2, h * height / 2))
                x_right, y_top = grid_center + np.array((w * width / 2, h * height / 2))
                x = np.linspace(x_left, x_right, grid_shape[0])
                y = np.linspace(y_bottom, y_top, grid_shape[1])
                mu_grid = np.array(np.meshgrid(x, y)).T.reshape(-1, 2)
                mu_grids.append(mu_grid)

            mu_arr = np.apply_along_axis(lambda row: np.stack(row), axis=0,
                                         arr=np.fromiter(itertools.product(*mu_grids), dtype=object, count=-1))

            for idx, mus in enumerate(tqdm(mu_arr, leave=True,
                                           desc=f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}')):

                if random_cov:
                    sigma = list_r(*tuple(
                        genPositiveDefMat(
                            dim=2,
                            covMethod='eigen',
                            rangeVar=c_r(0.1, 7)
                        ).rx2('Sigma')
                        for _ in range(num_clusters)
                    ))
                    init_params['sigma'] = sigma

                init_params['mu'] = list_r(*tuple(c_r(*mu.tolist()) for mu in mus))
                filename = f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}_rec{idx + 1}'
                fit(data, family, num_clusters,
                    stdout=False, plot=False, save=False,
                    filename=filename, **init_params)


def centers_circle_search(data, nums_clusters, families, num_iter=100, random_cov=False,
                          width=.3, height=.3, ratio=True, around_centers=True):
    if around_centers is False:
        raise NotImplementedError
    if ratio is False:
        raise NotImplementedError

    x_name, y_name = data.columns
    w = float(data[x_name].max()) - float(data[x_name].min())
    h = float(data[y_name].max()) - float(data[y_name].min())
    radius = (w * width + h * height) / 2

    for num_clusters in nums_clusters:

        for family in families:

            sigma = list_r(*tuple(matrix_r(c_r(*s), nrow=2) for s in [[1., 0., 0., 1.]] * num_clusters))
            shape = list_r(*tuple(c_r(*s) for s in [[0, 0]] * num_clusters))
            pii = c_r(0.334, 0.333, 0.333) if num_clusters == 3 else c_r(0.5, 0.5)

            init_params = {
                'mu': None,
                'sigma': sigma,
                'shape': shape,
                'pii': pii,
                'nu': 1
            }

            analysis = read_pickled(
                filename=f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}',
                path=os.path.join(ANALYSES_DIR_BINARY, f'{x_name}_{y_name}'))
            centers = np.array(analysis.rx2['mu']).tolist()

            for i in tqdm(
                    range(num_iter),
                    leave=True,
                    desc=f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}'
            ):

                r = [
                    radius * math.sqrt(random.random())
                    for _ in range(num_clusters)
                ]
                phi = [
                    2 * math.pi * random.random()
                    for _ in range(num_clusters)
                ]
                x = [
                    r[i] * math.cos(phi[i]) + centers[i][0]
                    for i in range(num_clusters)
                ]
                y = [
                    r[i] * math.sin(phi[i]) + centers[i][1]
                    for i in range(num_clusters)
                ]

                mus = list(zip(x, y))

                if random_cov:
                    sigma = list_r(*tuple(
                        genPositiveDefMat(
                            dim=2,
                            covMethod='eigen',
                            rangeVar=c_r(0.1, 7)
                        ).rx2('Sigma')
                        for _ in range(num_clusters)
                    ))
                    init_params['sigma'] = sigma

                init_params['mu'] = list_r(*tuple(c_r(*mu) for mu in mus))

                filename = f'{x_name}_{y_name}_{num_clusters}{SHORT_NAME_BY_FAMILY[family]}_circle_{i}'
                fit(data, family, num_clusters,
                    stdout=False, plot=False, save=True,
                    filename=filename, **init_params)
