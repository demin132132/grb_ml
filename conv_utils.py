import numpy as np
import pandas as pd
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri, numpy2ri
from rpy2.robjects.conversion import localconverter


def converted_to_r(obj_py):
    if isinstance(obj_py, pd.DataFrame):
        conv = pandas2ri.converter
    else:
        raise ValueError("Unsupported python object type for conversion to R.")
    with localconverter(robjects.default_converter + conv):
        obj_r = robjects.conversion.py2rpy(obj_py)
    return obj_r


def converted_to_pandas(data_r):
    with localconverter(robjects.default_converter + pandas2ri.converter):
        data_pandas = robjects.conversion.rpy2py(data_r)
    return data_pandas
